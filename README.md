
## How to send email with an attachment from commandline windows

 - use [mailsend](https://github.com/muquit/mailsend)

### Example with `mailsend`

#### Prerequisites:

1. Creaza-ti un account email, pentru exemplu va fi gmail account

  > Username: maxbelov312@gmail.com
  >
  > Password: OParolaPentruGmail
  >


2. Din cauza securitatii gmail nu permite sa trimita emails in mod automat de pe un account
  Pentru a scoate aceasta limitare, acceseaza (https://myaccount.google.com/lesssecureapps)
  bifeaza checkbox-ul:

  > Allow less secure apps: ON

#### Exemple email:


```sh
mailsend -q -to maxbelov312@gmail.com -from maxbelov312@gmail.com -starttls -port 587 -auth -smtp smtp.gmail.com -sub "test attachement file" -M "generated at: %DATE% %TIME%" +cc +bc -attach ".\attachmentfile.txt" -user maxbelov312@gmail.com -pass "OParolaPentruGmail"
```

Descifrarea argumentelor:
  - `-q`: quite, 
  - `-to`: recipientul
  - `-from`: adresatul, de dorit acelasi account cum si la user
  - `-starttls -port 587 -auth -smtp smtp.gmail.com`: setarile serverului smtp ale lui gmail
  - `-sub`: email subject
  - `-M`: textul mesajului (de dorit ceva sa fie implicit indicat)
  - `+cc +bc`: sa nu intrebe Carbon Copy (CC) si Blind Carbon Copy (BCC)
  - `-attach`: path-ul la fisier care trebuie atasat in email, in exemplu: este fisierul in acelasi directoriu "attachmentfile.txt"
  - `-user`: User account
  - `-pass`: User account

## How to send a file to ftp on Windows

- use [curl](https://curl.haxx.se/windows/)

### Example with `curl`


```sh
curl -T path_to_filename.txt ftp://username:password@ftp.server.com/backup/datetime_ip_address.txt
```